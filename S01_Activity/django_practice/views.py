from django.shortcuts import render, redirect

# Create your views here.

from django.http import HttpResponse

from .models import GroceryItem

from django.contrib.auth.models import User

from django.contrib.auth import authenticate, login, logout

from django.forms.models import model_to_dict

def index(request):
    groceryitem_list = GroceryItem.objects.all()
    context = {
        'groceryitem_list': groceryitem_list,
        'user': request.user
    }
    return render(request, "index.html", context)

def groceryitem(request, groceryitem_id):
    groceryitem = model_to_dict(GroceryItem.objects.get(pk = groceryitem_id))
    return render(request, "groceryitem.html", groceryitem)

def register(request):

    users = User.objects.all()
    is_user_registered = False

    user = User()
    user.username = "yhajed"
    user.first_name = "Yhaje"
    user.last_name = "de Leon"
    user.email = "yhajed@mail.com"
    user.set_password("yhaje1234")
    user.is_staff = False
    user.is_active = True

    for indiv_user in users:
        if indiv_user.username == user.username:
            is_user_registered = True
            break

    if is_user_registered == False:
        user.save()

    context = {
        'is_user_registered' : is_user_registered,
        'first_name': user.first_name,
        'last_name': user.last_name
    }

    return render(request, "register.html", context)

def change_password(request):

    is_user_authenticated = False

    user = authenticate(username = "yhajed", password = "yhaje1234")

    if user is not None:
        authenticated_user = User.objects.get(username = "yhajed")
        authenticated_user.set_password("yhaje12345")

        authenticated_user.save()

        is_user_authenticated = True

    context = {
        "is_user_authenticated" : is_user_authenticated
    }

    return render(request, "change_password.html", context)

def login_user(request):
    username = 'yhajed'
    password = 'yhaje12345'

    user = authenticate(username = username, password = password)

    if user is not None:
        login(request, user)
        return redirect("index")
    else:
        return render(request, "login.html")

def logout_user(request):
    logout(request)
    return redirect("index")